use std::{
    collections::{BTreeMap, HashSet},
    str::FromStr,
};

use super::incar_tag::IncarTag;

#[derive(Debug)]
pub struct Incar {
    pub tags: BTreeMap<String, IncarTag>,
}

impl Incar {
    pub fn update(&mut self, tag: IncarTag) -> Result<(), String> {
        Ok(())
    }
}

impl FromStr for Incar {
    type Err = String;
    /// Parse INCAR file from string.
    /// Ignores malformed lines, as VASP does.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut tags = BTreeMap::new();
        let incar_tags: Vec<IncarTag> = s.lines().filter_map(|line| line.parse::<IncarTag>().ok()).collect();
        let mut names = HashSet::new();
        for tag in incar_tags.iter() {
            let name = tag.get_name();
            names.insert(name.to_string());
        }
        Ok(Incar {
            tags: incar_tags,
            names,
        })
    }
}

impl ToString for Incar {
    fn to_string(&self) -> String {
        self.tags
            .iter()
            .map(|tag| tag.to_string())
            .collect::<Vec<String>>()
            .join("\n")
    }
}
