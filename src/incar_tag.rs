use super::all_tags::INCAR_TAGS;
use std::fmt::{Debug, Display};
use std::str::FromStr;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum IncarTagType {
    Bool,
    Integer,
    IntegerArray,
    Real,
    RealArray,
    String,
}

#[derive(Debug)]
pub enum IncarTag {
    Boolean(bool),
    Integer(i64),
    IntegerArray(Vec<i64>),
    Real(f64),
    RealArray(Vec<f64>),
    String(String),
}

impl FromStr for IncarTag {
    type Err = String;
    /// s: ex) "ISIF    =  3"
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, val) = {
            let mut iter = s
                .split('#')
                .next()
                .ok_or(String::from("Invalide tag format"))?
                .splitn(2, '=');
            (
                iter.next().ok_or(String::from("Invalide tag format"))?.trim(),
                iter.next().ok_or(String::from("Invalide tag format"))?.trim(),
            )
        };
        let tagtype = &INCAR_TAGS
            .get(name)
            .ok_or(format!("{} is not a valid INCAR tag", name))?;

        match tagtype {
            IncarTagType::String => Ok(IncarTag::String(val.into())),
            IncarTagType::Integer => Ok(IncarTag::Integer(parse_single_val(val)?)),
            IncarTagType::Real => Ok(IncarTag::Real(parse_single_val(val)?)),
            IncarTagType::Bool => Ok(IncarTag::Boolean(parse_bool(val)?)),
            IncarTagType::IntegerArray => Ok(IncarTag::IntegerArray(parse_array(val)?)),
            IncarTagType::RealArray => Ok(IncarTag::RealArray(parse_array(val)?)),
        }
    }
}

impl ToString for IncarTag {
    fn to_string(&self) -> String {
        match &self {
            IncarTag::Boolean(val) => {
                let bool_str = match val {
                    true => ".TRUE.",
                    false => ".FALSE.",
                };
                format!("{}", bool_str)
            }
            IncarTag::Integer(val) => format!("{}", val),
            IncarTag::Real(val) => format!("{}", val),
            IncarTag::String(val) => format!("{}", val),
            IncarTag::IntegerArray(val) => {
                let arr_repr = val
                    .into_iter()
                    .map(|x| format!("{}", x))
                    .collect::<Vec<String>>()
                    .join(" ");
                format!("{}", arr_repr)
            }
            IncarTag::RealArray(val) => {
                let arr_repr = val
                    .into_iter()
                    .map(|x| format!("{:.6}", x))
                    .collect::<Vec<String>>()
                    .join(" ");
                format!("{}", arr_repr)
            }
        }
    }
}

fn parse_single_val<T>(val_str: &str) -> Result<T, String>
where
    T: FromStr,
    <T as FromStr>::Err: Debug + Display,
{
    let val = val_str.parse::<T>().map_err(|e| e.to_string())?;
    Ok(val)
}

fn parse_bool(val_str: &str) -> Result<bool, String> {
    match val_str {
        "T" | "true" | "True" | ".TRUE." => Ok(true),
        "F" | "false" | "False" | ".FALSE." => Ok(false),
        _ => Err(format!("{} is not a valid boolean value", val_str)),
    }
}

fn parse_array<T>(val_str: &str) -> Result<Vec<T>, String>
where
    T: FromStr,
    <T as FromStr>::Err: Debug + Display,
{
    let mut iter = val_str.trim().split_whitespace();
    let mut vec = Vec::new();
    while let Some(val) = iter.next() {
        vec.push(val.trim().parse::<T>().map_err(|e| e.to_string())?);
    }
    Ok(vec)
}
