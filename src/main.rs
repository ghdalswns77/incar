pub mod all_tags;
pub mod incar;
mod incar_tag;

use incar::Incar;
use incar_tag::IncarTagType;
use std::collections::BTreeSet;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let incar = std::fs::read_to_string("INCAR").unwrap();
    println!("{}", incar);

    let incar: Incar = incar.parse().unwrap();
    println!("{:?}", incar);
    println!("{}", incar.to_string());

    let mut books = BTreeSet::new();
    books.insert("A Dance With Dragons");
    books.insert("To Kill a Mockingbird");
    books.insert("The Odyssey");
    books.insert("The Great Gatsby");
    books.insert("The Great Gatsby");
    println!("{:?}", books);
    Ok(())
}
